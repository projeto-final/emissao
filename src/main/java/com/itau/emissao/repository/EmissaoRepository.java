package com.itau.emissao.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.emissao.model.Emissao;

public interface EmissaoRepository extends CrudRepository<Emissao, Integer>{
	Emissao findById(int id);
	Emissao findByCpfCliente(String cpfCliente);
}
