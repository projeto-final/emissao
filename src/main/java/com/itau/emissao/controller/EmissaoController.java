package com.itau.emissao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.emissao.model.Emissao;
import com.itau.emissao.repository.EmissaoRepository;

@Controller
public class EmissaoController {
	@Autowired
	EmissaoRepository emissaoRepository;
	
	@RequestMapping(path="/emissao/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Emissao getEmissaoById(@PathVariable int id) {	
		
		return emissaoRepository.findById(id);
	}
	
// Exemplo de chamada do JSON!	
//	{
//		"nomeCliente": "Filipe",
//		"cpfCliente": "12345678910",
//		"idade": 28,
//		"produto": 28,
//		"produtoNome": "Seguro Itaú Vida",
//		"valorMensal": 128
//		
//	}
	
	@RequestMapping(path="/emissao", method=RequestMethod.POST)
	@ResponseBody
	@CrossOrigin
	public ResponseEntity<?>  emitirProposta(@RequestBody Emissao emissao) 
	{		
		return new ResponseEntity<Emissao> (emissaoRepository.save(emissao), HttpStatus.OK);
	}
	
	@RequestMapping(path="/emissao/cliente/{cpfCliente}", method=RequestMethod.GET)
	@ResponseBody
	public Emissao getEmissaoByCpfCliente(@PathVariable String cpfCliente) {	
		
		return emissaoRepository.findByCpfCliente(cpfCliente);
	}
}
